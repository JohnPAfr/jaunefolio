import './App.css';
import Header from './ui/header/Header';
import Home from './ui/home/Home';
import Technos from './ui/technos/Technos';
import Projects from './ui/projects/Projects';

function App() {
  return (
    <div className="app">
      <Header />
      <Home />
      <Technos />
      <Projects />
    </div>
  );
}

export default App;
