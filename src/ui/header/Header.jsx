import React from 'react'
import Navbar from './Navbar'
import './Header.css'

function Header() {
  return (
    <div className="header">
      <a href="/" className="header__logo">JONATHAN AURRY</a>
      <div className="header__separator"></div>
      <Navbar />
    </div>
  )
}

export default Header
