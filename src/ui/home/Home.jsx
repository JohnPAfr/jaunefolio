import React from 'react';
import './Home.css';

function Home() {
  return (
    <div className="home">
      <h1 className="home__title title-underscored">Développeur Web</h1>
      <p>Moi c’est Jonathan, on m’appelle plus souvent John. Je suis développeur web avec
une préférence pour le Front-end. Je réalise des applications web avec la stack MERN.
Je suis un amoureux de la nature, des animaux et de la cuisine !
J’aime aussi le jardinage, la lecture et le sport que je pratique quotidiennement le
matin pour bien commencer la journée.</p>
      <div className="home__buttons">
        <a href="#contact" className="btn btn-primary">
          CONTACTEZ-MOI
        </a>
        <a href="#projects" className="btn btn-secondary">
          Mes projets
        </a>
      </div>
    </div>
  )
}

export default Home
