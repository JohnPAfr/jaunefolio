import React from 'react'
import './Projects.css'

function Projects() {
  return (
    <div className="projects">
      <h1 className="projects__title title-underscored">Mes projets</h1>

      <div className="projects__container">
        <img src="./images/netflix.png" alt="screenshot netflix clone"/>
        <div className="projects__infos">
          <h2 className="projects__infosTitle"><a href="https://jaune-netflix-clone.web.app" target="_blank" rel="noreferrer">Clone Netflix</a></h2>
          <p className="projects__infosTechnos">react - firebase - rest api</p>
          <p className="projects__infosText">Création d’un clone de Netflix avec React. Les données proviennent du site TMDb et sont récuperé grâce à leur API REST</p>
          <a href="https://jaune-netflix-clone.web.app" target="_blank" rel="noreferrer" className="projects__infosLink">
            Voir le projet
            <svg width="34" height="24" viewBox="0 0 34 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M21.0205 16.8876C21.0776 16.8876 21.1342 16.8795 21.1868 16.8638C21.2394 16.848 21.2869 16.8249 21.3265 16.7958L27.5816 12.2172C27.6581 12.1614 27.7009 12.0869 27.7009 12.0093C27.7009 11.9317 27.6581 11.8571 27.5816 11.8014L21.2993 7.20417C21.2612 7.17411 21.215 7.14977 21.1632 7.13262C21.1115 7.11547 21.0555 7.10585 20.9984 7.10434C20.9413 7.10283 20.8844 7.10945 20.831 7.12382C20.7776 7.1382 20.7289 7.16002 20.6877 7.18798C20.6466 7.21594 20.6138 7.24948 20.5914 7.28658C20.5691 7.32368 20.5575 7.36358 20.5575 7.40391C20.5574 7.44423 20.5689 7.48415 20.5912 7.52127C20.6135 7.5584 20.6462 7.59196 20.6873 7.61997L26.6849 12.009L20.7145 16.3794C20.6566 16.4218 20.6177 16.4752 20.6025 16.5331C20.5874 16.591 20.5967 16.6507 20.6294 16.7049C20.662 16.7591 20.7165 16.8053 20.786 16.8378C20.8556 16.8703 20.9371 16.8876 21.0205 16.8876Z" fill="#494949"/>
              <path d="M6.7235 12.309H27.2765C27.3892 12.309 27.4973 12.2774 27.577 12.2211C27.6567 12.1649 27.7015 12.0885 27.7015 12.009C27.7015 11.9294 27.6567 11.8531 27.577 11.7969C27.4973 11.7406 27.3892 11.709 27.2765 11.709H6.7235C6.61078 11.709 6.50268 11.7406 6.42298 11.7969C6.34328 11.8531 6.2985 11.9294 6.2985 12.009C6.2985 12.0885 6.34328 12.1649 6.42298 12.2211C6.50268 12.2774 6.61078 12.309 6.7235 12.309Z" fill="#494949"/>
            </svg>
          </a>
        </div>
      </div>

      <div className="projects__container">
        <img src="./images/insta.png" alt="screenshot netflix clone"/>
        <div className="projects__infos">
          <h2 className="projects__infosTitle"><a href="https://jaunestagram.web.app" target="_blank" rel="noreferrer">Clone Instagram</a></h2>
          <p className="projects__infosTechnos">REACT - FIREBASE - NODEJS - EXPRESS - MONGODB</p>
          <p className="projects__infosText">Création d’un clone d’instagram :
            <ul>
              <li>Firebase pour l’authentification et le stockage</li>
              <li>Nodejs et Express pour le serveur</li>
              <li>Pusher pour une bdd en temps réel avec MongoDb</li>
            </ul></p>
          <a href="https://jaunestagram.web.app" target="_blank" rel="noreferrer" className="projects__infosLink">
            Voir le projet
            <svg width="34" height="24" viewBox="0 0 34 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M21.0205 16.8876C21.0776 16.8876 21.1342 16.8795 21.1868 16.8638C21.2394 16.848 21.2869 16.8249 21.3265 16.7958L27.5816 12.2172C27.6581 12.1614 27.7009 12.0869 27.7009 12.0093C27.7009 11.9317 27.6581 11.8571 27.5816 11.8014L21.2993 7.20417C21.2612 7.17411 21.215 7.14977 21.1632 7.13262C21.1115 7.11547 21.0555 7.10585 20.9984 7.10434C20.9413 7.10283 20.8844 7.10945 20.831 7.12382C20.7776 7.1382 20.7289 7.16002 20.6877 7.18798C20.6466 7.21594 20.6138 7.24948 20.5914 7.28658C20.5691 7.32368 20.5575 7.36358 20.5575 7.40391C20.5574 7.44423 20.5689 7.48415 20.5912 7.52127C20.6135 7.5584 20.6462 7.59196 20.6873 7.61997L26.6849 12.009L20.7145 16.3794C20.6566 16.4218 20.6177 16.4752 20.6025 16.5331C20.5874 16.591 20.5967 16.6507 20.6294 16.7049C20.662 16.7591 20.7165 16.8053 20.786 16.8378C20.8556 16.8703 20.9371 16.8876 21.0205 16.8876Z" fill="#494949"/>
              <path d="M6.7235 12.309H27.2765C27.3892 12.309 27.4973 12.2774 27.577 12.2211C27.6567 12.1649 27.7015 12.0885 27.7015 12.009C27.7015 11.9294 27.6567 11.8531 27.577 11.7969C27.4973 11.7406 27.3892 11.709 27.2765 11.709H6.7235C6.61078 11.709 6.50268 11.7406 6.42298 11.7969C6.34328 11.8531 6.2985 11.9294 6.2985 12.009C6.2985 12.0885 6.34328 12.1649 6.42298 12.2211C6.50268 12.2774 6.61078 12.309 6.7235 12.309Z" fill="#494949"/>
            </svg>
          </a>
        </div>
      </div>

      <div className="projects__container">
        <img src="./images/amazon.png" alt="screenshot netflix clone"/>
        <div className="projects__infos">
          <h2 className="projects__infosTitle"><a href="https://jaunezon.web.app" target="_blank" rel="noreferrer">Clone Amazon</a></h2>
          <p className="projects__infosTechnos">react - firebase - stripe</p>
          <p className="projects__infosText">Création d’un clone d’Amazon :
            <ul>
              <li>Utilisation de Firebase pour l’authentification, la bdd en temps réel ainsi que
les fonctions cloud pour le backend</li>
              <li>API Stripe pour le paiement</li>
              <li>Context API de React pour la gestion les informations à travers l’application</li>
            </ul></p>
          <a href="https://jaunezon.web.app" target="_blank" rel="noreferrer" className="projects__infosLink">
            Voir le projet
            <svg width="34" height="24" viewBox="0 0 34 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M21.0205 16.8876C21.0776 16.8876 21.1342 16.8795 21.1868 16.8638C21.2394 16.848 21.2869 16.8249 21.3265 16.7958L27.5816 12.2172C27.6581 12.1614 27.7009 12.0869 27.7009 12.0093C27.7009 11.9317 27.6581 11.8571 27.5816 11.8014L21.2993 7.20417C21.2612 7.17411 21.215 7.14977 21.1632 7.13262C21.1115 7.11547 21.0555 7.10585 20.9984 7.10434C20.9413 7.10283 20.8844 7.10945 20.831 7.12382C20.7776 7.1382 20.7289 7.16002 20.6877 7.18798C20.6466 7.21594 20.6138 7.24948 20.5914 7.28658C20.5691 7.32368 20.5575 7.36358 20.5575 7.40391C20.5574 7.44423 20.5689 7.48415 20.5912 7.52127C20.6135 7.5584 20.6462 7.59196 20.6873 7.61997L26.6849 12.009L20.7145 16.3794C20.6566 16.4218 20.6177 16.4752 20.6025 16.5331C20.5874 16.591 20.5967 16.6507 20.6294 16.7049C20.662 16.7591 20.7165 16.8053 20.786 16.8378C20.8556 16.8703 20.9371 16.8876 21.0205 16.8876Z" fill="#494949"/>
              <path d="M6.7235 12.309H27.2765C27.3892 12.309 27.4973 12.2774 27.577 12.2211C27.6567 12.1649 27.7015 12.0885 27.7015 12.009C27.7015 11.9294 27.6567 11.8531 27.577 11.7969C27.4973 11.7406 27.3892 11.709 27.2765 11.709H6.7235C6.61078 11.709 6.50268 11.7406 6.42298 11.7969C6.34328 11.8531 6.2985 11.9294 6.2985 12.009C6.2985 12.0885 6.34328 12.1649 6.42298 12.2211C6.50268 12.2774 6.61078 12.309 6.7235 12.309Z" fill="#494949"/>
            </svg>
          </a>
        </div>
      </div>
    </div>
  )
}

export default Projects
