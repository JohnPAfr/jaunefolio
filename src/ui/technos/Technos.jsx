import React from 'react';
import './Technos.css';
import { HtmlIcon, CssIcon, JsIcon, ReactIcon, NodeIcon, FirebaseIcon, GitIcon } from '../icons/Icons'

function Technos() {

  return (
    <div className="technos">
      <h1 className="technos__title title-underscored">Les technos que j'utilise</h1>
      <div className="technos__container">
        <HtmlIcon />
        <CssIcon />
        <JsIcon />
        <ReactIcon />
        <NodeIcon />
        <FirebaseIcon />
        <GitIcon />
      </div>
    </div>
  )
}

export default Technos
